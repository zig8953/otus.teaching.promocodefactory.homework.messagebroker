﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class ValueEntered
    {
        public Guid PartnerManagerId { get; set; }
    }
}
