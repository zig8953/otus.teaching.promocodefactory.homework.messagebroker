﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Consumers;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;


namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        readonly IPublishEndpoint _publishEndpoint;

        public AdministrationGateway(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }
        
        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            await _publishEndpoint.Publish<ValueEntered>(new
            {
                PartnerManagerId = partnerManagerId
            });
        }
    }
}